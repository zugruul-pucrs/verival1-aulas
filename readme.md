# Projetos
## Como rodar:
+ Tenha node instalado: [Node](https://nodejs.org/en/) ou [nvm](https://github.com/creationix/nvm);
    + Opcional: tenha yarn instalado: ```$ npm i -g yarn```;
+ Entre na aula em específico desejada usando cd (e.g. ```cd aula-26```);
+ Rode um ```$ npm i``` para instalar dependências, ou se preferir use yarn ```$ yarn install```;
+ Rode um ```$ npm test```;
+ Voila.


## Grupos em Datas
| Aula |      Grupo       |
|------|------------------|
|03-21 |Leonardo & Octavio|
|03-28*|Leonardo & Phillip|
* Tarefa se estendeu por uma ou mais aulas. Consta data que encerrou-se a tarefa.