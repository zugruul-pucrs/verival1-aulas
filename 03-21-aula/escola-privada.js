function contemCaractereEspecial(string) {
	return string.match(/^[^a-zA-Z0-9]+$/) ? true : false;
}

function atribuirNota(aluno, nota) {
	if(!aluno || !aluno.length) throw new Error('Nome em branco')
	if(contemCaractereEspecial(aluno)) throw new Error('Nome com caracteres especiais')
	if(nota < 0) throw new Error('Percentual de acerto negativo')
	if(nota > 100) throw new Error('Percentual de acerto superior a 100%')

	if(nota <= 39) return 'D'
	if(nota <= 59) return 'C'
	if(nota <= 70) return 'B'
	if(nota <= 100) return 'A'
}

module.exports.atribuirNota = atribuirNota