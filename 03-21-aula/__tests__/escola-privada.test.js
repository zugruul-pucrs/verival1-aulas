const { atribuirNota } = require('../escola-privada')
const {  } = require('jest')

describe('Teste de atribuição', () => {
	
	// Válido
	it('Octavio e percentual acertos >= 0 <= 100', () => {
		const resultado = atribuirNota("Octavio", 66)
		expect(resultado).toEqual('B')
	})

	// Inválido
	it('Nome em branco e percentual de acertos >= 0 <= 100', () => {
		const resultado = atribuirNota("Octavio", 66)
		expect(resultado).toEqual('B')
	})

	// Inválido
	it('Oct$avio e  >= 0 <= 100', () => {
		try {
			const resultado = atribuirNota("Oct$avio", 66)
		} catch (err) {
			expect(err).toBeDefined();
		}
	})

	// Inválido
	it('Octavio  e percentual de acertos < 0', () => {
		try {
			const resultado = atribuirNota("Octavio", -1)
		} catch (err) {
			expect(err).toBeDefined();
		}
	})

	// Inválido
	it('Octavio e percentual de acertos > 0', () => {
		try {
			const resultado = atribuirNota("Octavio", 140)
		} catch (err) {
			expect(err).toBeDefined();
		}
	})
})