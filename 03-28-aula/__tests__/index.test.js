require('jest')
const { color, subscription, Student, printCode } = require('../index')

describe('color test', () => {
    test('red test', () => {
        expect(color(1)).toBe('red')
    })

    test('yellow test', () => {
        expect(color(2)).toBe('yellow')
    })

    test('blue test', () => {
        expect(color(3)).toBe('blue')
    })

    test('violet test', () => {
        expect(color(4)).toBe('violet')
    })

    test('error test', () => {
        let expectedError

        try {
            color(0)
        } catch (err) {
            expectedError = err 
        }

        expect(expectedError).toBeDefined()
    })
})

describe('subscription', () => {
    test('payment outside of the range and correct status', () => {
        const result = subscription(0, 'estudante')
        
        expect(result.code).toBe(1)
        expect(result.message).toEqual('value error')
    })

    test('payment outside of the range and wrong status', () => {
        const result = subscription(100000, 'mafioso')
        
        expect(result.code).toBe(1)
        expect(result.message).toEqual('value error')
    })

    test('correct payment and wrong status', () => {
        const result = subscription(666, 'sausage')
        
        expect(result.code).toBe(2)
        expect(result.message).toEqual('incorrect status')
    })

    test('correct payment and correct status', () => {
        const result = subscription(666, 'aposentado')
        
        expect(result.code).toBe(0)
        expect(result.message).toEqual('success')
    })

})

describe('Student status', () => {
    const student = new Student()

    test('has G2, avg 7 or over', () => {
        const result = student.status(7, true)

        expect(result).toEqual('approved')
    })

    test('has G2, avg bellow 4', () => {
        const result = student.status(4, true)

        expect(result).toEqual('reproved')
    })

    test('no G2, avg bellow 5', () => {
        const result = student.status(4.9, false)

        expect(result).toEqual('reproved')
    })

    test('no G2, avg 5 or more', () => {
        const result = student.status(5, false)

        expect(result).toEqual('approved')
    })
})

describe('Print code', () => {
    test('character wrong', () => {
        const result = printCode('J7')

        expect(result.message).toEqual('X')
    })

    test('number wrong', () => {
        const result = printCode('BJ')

        expect(result.message).toEqual('Y')
    })
    test('correct message', () => {
        const result = printCode('B3')

        expect(result.message).toEqual('B3')
    })
})