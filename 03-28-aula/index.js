function color(code) {
    switch(code) {
        case 1: return 'red'
        case 2: return 'yellow'
        case 3: return 'blue'
        case 4: return 'violet'
        default: throw new Error('invalid color code input')
    }
}

function subscription(payment, status) {
    if(payment > 99999 || payment < 0.01) return { code: 1, message: 'value error' }
    if(!['regular', 'estudante', 'aposentado', 'vip'].includes(status.toLowerCase())) return { code: 2, message: 'incorrect status' }
    return { code: 0, message: 'success' }
}

class Student {
    status(average, hasG2) {
        if(hasG2 && average >= 7) return 'approved'
        if(hasG2 && average <= 4) return 'reproved'
        if(!hasG2 && average >= 5) return 'approved'
        return 'reproved'
    }
}

function printCode(input) {
    if(input.length > 2) throw new Error('Invalid number of inputs')
    if(!['A', 'B'].includes(input[0])) return { message: 'X' }
    if(isNaN(input[1])) return { message: 'Y' }
    return { message: input, status: 'file updated' }
}

module.exports.color = color
module.exports.subscription = subscription
module.exports.Student = Student
module.exports.printCode = printCode